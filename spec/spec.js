/*
 * A fake `NewMiddleware` to make it easier for us to test. `NewProcessRequest`
 * and `ReturnData` are required by the fake `TykJS` to hook in the middleware,
 * and then return the correctly formatted response object to TYK's JSVM.
 */
var testHarness = {
	// captured to allow us to invoke it separately
	callback: null,
	NewProcessRequest: function(callback) {
		this.callback = callback;
	},
	ReturnData: function(request, metadata) {
		return {
			Request: request,
			SessionMeta: metadata
		};
	}
};

/*
 * Fake the call to create a `NewMiddleware`:
 * `samplePostProcessMiddleware = new TykJS.TykMiddleware.NewMiddleware({});`
 */
TykJS = {
	TykMiddleware: {
		NewMiddleware: function() {
			return testHarness;
		}
	}
};

/*
 * Spy on the `log` function to ensure that it's called correctly. Don't bother
 * stubbing as we're only expecting calls to it, not return values from it.
 */
log = jasmine.createSpy('log()');

// https://stackoverflow.com/a/5533226/2257038
function obj_length(obj) {
	return Object.keys(obj).length;
}

describe('samplePostProcessMiddleware', function() {
	var req = null;
	beforeEach(function() {
		// fake out the `req` object that gets passed into the middleware
		// function for the requirements of our test the schema of the
		// `Request` object can be found at
		// https://tyk.io/docs/customise-tyk/plugins/javascript-middleware/middleware-scripting-guide/#the-request-object
		req = {
			AddParams: {},
			DeleteParams: [],
			SetHeaders: {},
			Body: ''
		}
	});

	var samplePostProcessMiddleware = require('../samplePostProcessMiddleware');
	it('affects our incoming request', function() {
		// given

		// when
		var ret = testHarness.callback(req, {});

		// then
		expect(log).toHaveBeenCalledWith("Sample POST middleware initialised");
		expect(log).toHaveBeenCalledWith("Running sample  POST PROCESSOR JSVM middleware");

		expect(obj_length(ret.Request.SetHeaders)).toEqual(1);
		expect(ret.Request.SetHeaders['User-Agent']).toEqual('Tyk-Custom-JSVM-Middleware');

		expect(obj_length(ret.Request.AddParams)).toEqual(1);
		expect(ret.Request.AddParams['test_param']).toEqual('My Teapot2');

		expect(obj_length(ret.Request.DeleteParams)).toEqual(1);
		expect(ret.Request.DeleteParams[0]).toEqual('delete_me');

		expect(ret.Request.Body).toEqual('New Request body2');

		expect(ret.SessionMeta).toEqual({});
	});
});
